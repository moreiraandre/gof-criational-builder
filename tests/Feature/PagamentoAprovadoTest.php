<?php

namespace Tests\Feature;

use Builder\DtoBuilder;
use Builder\NotificacaoHtmlBuilder;
use Director;
use PessoaDto;
use PHPUnit\Framework\TestCase;
use Produto\ProcessamentoDto;

class PagamentoAprovadoTest extends TestCase
{
    public function testSucesso()
    {
        $pessoa = new PessoaDto();
        $pessoa->pronomeTratamento = 'Sra.';
        $pessoa->primeiroNome = 'Vitória';
        $pessoa->ultimoNome = 'Carolina Alves Pereira';
        $pessoa->endereco = 'Rua Principal, SN Centro';

        $dtoBuilder = new DtoBuilder($pessoa);
        $notificacaoHtmlBuilder = new NotificacaoHtmlBuilder($pessoa);

        $director = new Director();

        $director->iniciar($dtoBuilder)->setTransacaoAprovada();
        $director->iniciar($notificacaoHtmlBuilder)->setTransacaoAprovada();

        $processamentoDto = $dtoBuilder->getResultado();
        $notificacaoHtml = $notificacaoHtmlBuilder->getResultado();

        $this->assertInstanceOf(ProcessamentoDto::class, $processamentoDto);
        $this->assertTrue(str_contains($notificacaoHtml, 'APROVADO'));
    }
}