<?php

use Builder\BuilderInterface;

class Director
{
    private BuilderInterface $builder;

    public function iniciar(BuilderInterface $builder): Director
    {
        $this->builder = $builder;
        return $this;
    }

    public function setTransacaoAprovada()
    {
        $this->builder->iniciar();
        $this->builder->setNome();
        $this->builder->setTransacaoAprovada();
        $this->builder->setEndereco();
    }

    public function setTransacaoRecusada()
    {
        $this->builder->iniciar();
        $this->builder->setNome();
        $this->builder->setTransacaoRecusada();
    }
}