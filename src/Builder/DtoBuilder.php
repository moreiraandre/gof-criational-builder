<?php

namespace Builder;

use PessoaDto;
use Produto\ProcessamentoDto;

class DtoBuilder implements BuilderInterface
{
    private ProcessamentoDto $processamentoDto;

    public function __construct(private PessoaDto $pessoaDto)
    {
    }

    public function iniciar(): BuilderInterface
    {
        $this->processamentoDto = new ProcessamentoDto();
        return $this;
    }


    public function setNome(): BuilderInterface
    {
        $this->processamentoDto->nome =
            "{$this->pessoaDto->pronomeTratamento} "
            . "{$this->pessoaDto->primeiroNome} "
            . "{$this->pessoaDto->ultimoNome}";

        return $this;
    }

    public function setEndereco(): BuilderInterface
    {
        $this->processamentoDto->endereco = $this->pessoaDto->endereco;
        return $this;
    }

    public function setTransacaoAprovada(): BuilderInterface
    {
        $this->processamentoDto->setTipoTeansacao('APROVADA');
        return $this;
    }

    public function setTransacaoRecusada(): BuilderInterface
    {
        $this->processamentoDto->setTipoTeansacao('RECUSADA');
        return $this;
    }

    public function getResultado(): ProcessamentoDto
    {
        return $this->processamentoDto;
    }
}