<?php

namespace Builder;

use PessoaDto;

interface BuilderInterface
{
    public function __construct(PessoaDto $pessoaDto);
    public function iniciar(): BuilderInterface;
    public function setNome(): BuilderInterface;
    public function setEndereco(): BuilderInterface;
    public function setTransacaoAprovada(): BuilderInterface;
    public function setTransacaoRecusada(): BuilderInterface;
}