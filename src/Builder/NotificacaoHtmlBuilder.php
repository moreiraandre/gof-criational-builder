<?php

namespace Builder;

use PessoaDto;

class NotificacaoHtmlBuilder implements BuilderInterface
{
    private string $texto;

    public function __construct(private PessoaDto $pessoaDto)
    {
    }

    public function iniciar(): BuilderInterface
    {
        $this->texto = '';
        return $this;
    }


    public function setNome(): BuilderInterface
    {
        $this->texto .=
            'Olá <h1>'
            . "{$this->pessoaDto->pronomeTratamento} "
            . "{$this->pessoaDto->primeiroNome} "
            . "{$this->pessoaDto->ultimoNome}"
            . '</h1>.';

        return $this;
    }

    public function setEndereco(): BuilderInterface
    {
        $this->pularLinha();
        $this->texto .= "A entrega será realizada neste endereço: <i>{$this->pessoaDto->endereco}</i>";
        return $this;
    }

    public function setTransacaoAprovada(): BuilderInterface
    {
        $this->pularLinha();
        $this->texto .= <<<MENSAGEM
        Vinhemos por meio deste, informar que o seu pagamento foi <strong>APROVADO</strong>. Parabéns!'
        MENSAGEM;

        return $this;
    }

    public function setTransacaoRecusada(): BuilderInterface
    {
        $this->pularLinha();
        $this->texto .= <<<MENSAGEM
        Vinhemos por meio deste, informar que o seu pagamento foi <strong>RECUSADO</strong>. 
        Por favor, tente novamente com outra forma de pagamento.
        MENSAGEM;

        return $this;
    }

    private function pularLinha()
    {
        $this->texto .= '<br>';
    }

    public function getResultado(): string
    {
        return $this->texto;
    }
}