<?php

class PessoaDto
{
    public string $pronomeTratamento;
    public string $primeiroNome;
    public string $ultimoNome;
    public string $endereco;
}