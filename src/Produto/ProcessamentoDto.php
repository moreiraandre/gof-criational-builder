<?php

namespace Produto;

use Exception;

class ProcessamentoDto
{
    public string $nome;
    public string $endereco;
    private string $tipoTransacao;

    public function setTipoTeansacao(string $tipoTransacao): void
    {
        if (!in_array($tipoTransacao, ['RECUSADA', 'APROVADA'])) {
            throw new Exception('Tipo inválido.');
        }
        $this->tipoTransacao = $tipoTransacao;
    }

    public function getTipoTeansacao(): string
    {
        return $this->tipoTransacao;
    }
}