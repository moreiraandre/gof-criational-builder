# GOF - Criacional - Builder
Este projeto tem o intuito de estudar o padrão `Builder` do grupo `Criacional` dos padrões de projeto GOF.

[Referência](https://refactoring.guru/pt-br/design-patterns/builder)

Neste projeto foi implementado um `gerador de um objeto DTO, e uma mensagem para informar ao usuário o status do pagamento`, 
o objeto DTO serviria para as demais etapas do processamento e a mensagem em HTML serviria para enviar um email ou gerar 
uma notificação na tela do sistema, por exemplo.

# Docker
## Criar a imagem
```bash
docker-compose build --no-cache # Apenas uma vez
```

## Entrar no terminal do container
```bash
docker-compose run --rm workspace bash
```

# Instalar dependências composer
```bash
composer install
```

# Exemplos
```bash
php exemplos/app.php
```
> Será solicitada a resposta `a/r` para a pergunta `Gerar dados para pagamento aprovado(a) ou recusado(r)?`, informe e pressione `ENTER`.

# Testes Automatizados
```bash
bin/phpunit --debug --coverage-text
```