<?php

require 'vendor/autoload.php';

$resposta = readline('Gerar dados para pagamento aprovado(a) ou recusado(r)?');

if (!in_array($resposta, ['a', 'r'])) {
    echo PHP_EOL . 'Resposta inválida.' . PHP_EOL;
    return;
}

$pessoa = new PessoaDto();
$pessoa->pronomeTratamento = 'Sra.';
$pessoa->primeiroNome = 'Vitória';
$pessoa->ultimoNome = 'Carolina Alves Pereira';
$pessoa->endereco = 'Rua Principal, SN Centro';

$dtoBuilder = new \Builder\DtoBuilder($pessoa);
$notificacaoHtmlBuilder = new \Builder\NotificacaoHtmlBuilder($pessoa);

$director = new Director();

if ($resposta == 'a') {
    $director->iniciar($dtoBuilder)->setTransacaoAprovada();
    $director->iniciar($notificacaoHtmlBuilder)->setTransacaoAprovada();
} else {
    $director->iniciar($dtoBuilder)->setTransacaoRecusada();
    $director->iniciar($notificacaoHtmlBuilder)->setTransacaoRecusada();
}

$processamentoDto = $dtoBuilder->getResultado();
$notificacaoHtml = $notificacaoHtmlBuilder->getResultado();

echo PHP_EOL . 'A mensagem em HTML é:' . PHP_EOL;
echo $notificacaoHtml . PHP_EOL;

echo PHP_EOL . 'O objeto DTO é:' . PHP_EOL;
var_dump($processamentoDto);
